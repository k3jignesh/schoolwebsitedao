package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Labs_Join_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Labs_Join_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_labs_join_tb_dao {

	public void insertData(School_Labs_Join_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Labs_Join_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Labs_Join_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Labs_Join_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Labs_Join_Tb");
		List<School_Labs_Join_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Labs_Join_Tb> selectData(School_Labs_Join_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Labs_Join_Tb WHERE slj_id" + object.getSlj_id());
		List<School_Labs_Join_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
