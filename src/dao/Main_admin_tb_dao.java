package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.Main_Admin_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class Main_admin_tb_dao {

	public void insertData(Main_Admin_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(Main_Admin_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(Main_Admin_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	
	public List<Main_Admin_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Main_Admin_Tb");
		List<Main_Admin_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Main_Admin_Tb> selectData(Main_Admin_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Main_Admin_Tb WHERE ma_id" + object.getMa_id());
		List<Main_Admin_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Main_Admin_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<Main_Admin_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
