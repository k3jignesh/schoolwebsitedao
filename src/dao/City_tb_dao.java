package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.City_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class City_tb_dao {
	public void insertData(City_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(City_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(City_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<City_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM City_Tb");
		List<City_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<City_Tb> selectData(City_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM City_Tb WHERE c_id" + object.getC_id());
		List<City_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<City_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<City_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
