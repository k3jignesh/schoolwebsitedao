package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.Subject_Stream_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.Subject_Stream_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class Subject_stream_tb_dao {

	public void insertData(Subject_Stream_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(Subject_Stream_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(Subject_Stream_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<Subject_Stream_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Subject_Stream_Tb");
		List<Subject_Stream_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Subject_Stream_Tb> selectData(Subject_Stream_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Subject_Stream_Tb WHERE subs_id" + object.getSubs_id());
		List<Subject_Stream_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
