package dao;

import java.util.ArrayList;
import java.util.List;


import org.bitbucket.k3jignesh.hibernatetestPOJOs.Class_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class Class_tb_dao {

	public void insertData(Class_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(Class_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(Class_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<Class_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Class_Tb");
		List<Class_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Class_Tb> selectData(Class_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Class_Tb WHERE sc_id" + object.getSc_id());
		List<Class_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Class_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<Class_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
