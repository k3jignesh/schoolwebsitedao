package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Topper_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Topper_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_topper_tb_dao {

	public void insertData(School_Topper_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Topper_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Topper_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Topper_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Topper_Tb");
		List<School_Topper_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Topper_Tb> selectData(School_Topper_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Topper_Tb WHERE st_id" + object.getSt_id());
		List<School_Topper_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
