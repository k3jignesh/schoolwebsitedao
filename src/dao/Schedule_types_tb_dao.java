package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.Schedule_Types_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class Schedule_types_tb_dao {

	public void insertData(Schedule_Types_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(Schedule_Types_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(Schedule_Types_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	
	public List<Schedule_Types_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Schedule_Types_Tb");
		List<Schedule_Types_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Schedule_Types_Tb> selectData(Schedule_Types_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Schedule_Types_Tb WHERE sst_id" + object.getSst_id());
		List<Schedule_Types_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Schedule_Types_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<Schedule_Types_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
