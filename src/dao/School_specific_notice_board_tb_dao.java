package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Specific_Notice_Board_Tb ;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Specific_Notice_Board_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_specific_notice_board_tb_dao {

	public void insertData(School_Specific_Notice_Board_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Specific_Notice_Board_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Specific_Notice_Board_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Specific_Notice_Board_Tb > selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Specific_Notice_Board_Tb ");
		List<School_Specific_Notice_Board_Tb > tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Specific_Notice_Board_Tb > selectData(School_Specific_Notice_Board_Tb  object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Specific_Notice_Board_Tb  WHERE ssnb_id" + object.getSsnb_id());
		List<School_Specific_Notice_Board_Tb > tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	
}
