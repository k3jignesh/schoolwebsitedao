package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Admission_Enquiry_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_admission_enquiry_tb_dao {

	public void insertData(School_Admission_Enquiry_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Admission_Enquiry_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Admission_Enquiry_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Admission_Enquiry_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Admission_Enquiry_Tb");
		List<School_Admission_Enquiry_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Admission_Enquiry_Tb> selectData(School_Admission_Enquiry_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Admission_Enquiry_Tb WHERE sae_id" + object.getSae_id());
		List<School_Admission_Enquiry_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Admission_Enquiry_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<School_Admission_Enquiry_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	
}
