package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.Disability_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class Disability_tb_dao {

	public void insertData(Disability_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(Disability_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(Disability_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<Disability_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Disability_Tb");
		List<Disability_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<Disability_Tb> selectData(Disability_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Disability_Tb WHERE d_id" + object.getD_id());
		List<Disability_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
