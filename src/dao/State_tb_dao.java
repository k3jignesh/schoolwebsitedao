package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.State_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.State_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class State_tb_dao {

	public void insertData(State_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(State_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(State_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<State_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM State_Tb");
		List<State_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<State_Tb> selectData(State_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM State_Tb WHERE s_id" + object.getS_id());
		List<State_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
