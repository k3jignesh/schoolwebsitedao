package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Schedule_Types_Join_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Schedule_Types_Join_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_schedule_types_join_tb_dao {

	public void insertData(School_Schedule_Types_Join_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Schedule_Types_Join_Tb  object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Schedule_Types_Join_Tb  object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Schedule_Types_Join_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Schedule_Types_Join_Tb");
		List<School_Schedule_Types_Join_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Schedule_Types_Join_Tb> selectData(School_Schedule_Types_Join_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Schedule_Types_Join_Tb WHERE sstj_id" + object.getSstj_id());
		List<School_Schedule_Types_Join_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
