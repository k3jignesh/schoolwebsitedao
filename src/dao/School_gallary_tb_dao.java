package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Gallary_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_gallary_tb_dao {

	public void insertData(School_Gallary_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Gallary_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Gallary_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Gallary_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Gallary_Tb");
		List<School_Gallary_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Gallary_Tb> selectData(School_Gallary_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Gallary_Tb WHERE sg_id" + object.getSg_id());
		List<School_Gallary_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	
	public List<School_Gallary_Tb> customSelect(String whereClause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM Lab_Types_Tb WHERE " + whereClause);
		List<School_Gallary_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
