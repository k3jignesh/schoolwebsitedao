package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Principal_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Principal_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_principal_tb_dao {

	public void insertData(School_Principal_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Principal_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Principal_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Principal_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Principal_Tb");
		List<School_Principal_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Principal_Tb> selectData(School_Principal_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Principal_Tb WHERE sp_id" + object.getSp_id());
		List<School_Principal_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
