package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Medium_Tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_Medium_Tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_medium_tb_dao {

	public void insertData(School_Medium_Tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_Medium_Tb  object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_Medium_Tb  object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_Medium_Tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Medium_Tb");
		List<School_Medium_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_Medium_Tb> selectData(School_Medium_Tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_Medium_Tb WHERE sm_id" + object.getSm_id());
		List<School_Medium_Tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	
}
