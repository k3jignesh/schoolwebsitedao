package dao;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_User_Type_tb;
import org.bitbucket.k3jignesh.hibernatetestPOJOs.School_User_Type_tb;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class School_user_type_tb_dao {

	public void insertData(School_User_Type_tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.save(object);
		transaction.commit();
	}
	public void updateData(School_User_Type_tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.update(object);
		transaction.commit();
	}
	public void deleteData(School_User_Type_tb object) {
		Session session = Commons.getSession();
		Transaction transaction = Commons.getTransaction();
		session.delete(object);
		transaction.commit();
	}
	public List<School_User_Type_tb> selectAllData() {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_User_Type_tb");
		List<School_User_Type_tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	public List<School_User_Type_tb> selectData(School_User_Type_tb object) {
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM School_User_Type_tb WHERE sut_id" + object.getSut_id());
		List<School_User_Type_tb> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
}
