package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class CustomQueries {

	public List<Object> customSelect(String clause){
		Session session = Commons.getSession();
		Query query = session.createQuery("FROM " + clause);
		List<Object> tempList = new ArrayList();
		tempList = query.list();
		return tempList;	
	}
	
}
